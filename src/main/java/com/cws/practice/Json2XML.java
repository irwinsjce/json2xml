package com.cws.practice;

import com.cws.practice.factory.ConverterFactory;

import java.util.Scanner;

public class Json2XML {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Please enter Json file path: ");
            String jsonFilePath = scanner.nextLine();

            System.out.print("Please enter XML File Path: ");
            String xmlFilePath = scanner.nextLine();

            new ConverterFactory().getConverter().createXMLJSONConverter(jsonFilePath, xmlFilePath);
        }

    }
}
