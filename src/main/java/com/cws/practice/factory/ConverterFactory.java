package com.cws.practice.factory;

import com.cws.practice.converter.Json2XMLConverter;
import com.cws.practice.converter.XMLJSONConverterl;

public class ConverterFactory {
    public XMLJSONConverterl getConverter(){
        return new Json2XMLConverter();
    }
}
