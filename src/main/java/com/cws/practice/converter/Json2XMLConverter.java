package com.cws.practice.converter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.Iterator;
import java.util.Objects;

public class Json2XMLConverter implements XMLJSONConverterl{
    @Override
    public void createXMLJSONConverter(String jsonName, String xmlName) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element rootElement = document.createElement("object");
            document.appendChild(rootElement);
            InputStream in = getClass().getClassLoader().getResourceAsStream(jsonName);
            if(Objects.isNull(in)) {
                throw new FileNotFoundException("JSON file coule not be found");
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            Object parsedObj = new JSONParser().parse(sb.toString());
            JSONObject jsonObject = (JSONObject) parsedObj;
            Iterator<String> keys = jsonObject.keySet().iterator();
            while(keys.hasNext()) {
                String key = keys.next();
                buildXMLFromJSON(jsonObject, key, document, rootElement);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            //StreamResult streamResult = new StreamResult(new File(xmlName));
            StreamResult streamResult = new StreamResult(System.out);
            transformer.transform(domSource, streamResult);
        } catch (IOException | ParseException | ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }

    }

    private void buildXMLFromJSON(JSONObject jsonObject, String key, Document document, Element rootElement) {
        if(Objects.isNull(jsonObject.get(key))) {
            buildXMLForNull(key, document, rootElement);
        }
        if(jsonObject.get(key) instanceof JSONObject) {
            buildXMLForJSONObject((JSONObject)jsonObject.get(key), key, document, rootElement);
        }
        if(jsonObject.get(key) instanceof JSONArray) {
            buildXMLForJSONArray((JSONArray)jsonObject.get(key), key, document, rootElement);
        }
        if(jsonObject.get(key) instanceof String) {
            buildXMLForString((String)jsonObject.get(key), key, document, rootElement);
        }
        if(jsonObject.get(key) instanceof Number) {
            buildXMLForNumber((Number)jsonObject.get(key), key, document, rootElement);
        }
        if(jsonObject.get(key) instanceof Boolean) {
            buildXMLForBoolean((Boolean)jsonObject.get(key), key, document, rootElement);
        }
    }

    private void buildXMLForNull(String key, Document document, Element rootElement) {
        Element element = createElementAndAttributes(document, key, "null");
        element.appendChild(document.createTextNode(null));
        rootElement.appendChild(element);
    }

    private void buildXMLForBoolean(Boolean booleanVal, String key, Document document, Element rootElement) {
        Element element = createElementAndAttributes(document, key, "boolean");
        element.appendChild(document.createTextNode(String.valueOf(booleanVal)));
        rootElement.appendChild(element);
    }

    private void buildXMLForNumber(Number number, String key, Document document, Element rootElement) {
        Element element = createElementAndAttributes(document, key, "number");
        element.appendChild(document.createTextNode(String.valueOf(number)));
        rootElement.appendChild(element);
    }

    private void buildXMLForString(String strVal, String key, Document document, Element rootElement) {
        Element element = createElementAndAttributes(document, key, "string");
        element.appendChild(document.createTextNode(strVal));
        rootElement.appendChild(element);
    }

    private void buildXMLForJSONArray(JSONArray jsonArray, String key, Document document, Element rootElement) {

        Element element = createElementAndAttributes(document, key, "array");
        rootElement.appendChild(element);
        buildXMLForJSONArray(jsonArray, document, element);
    }

    private void buildXMLForJSONArray(JSONArray jsonArray, Document document, Element element) {
        jsonArray.forEach( ele -> {
            if(Objects.isNull(ele)) {
                buildXMLForNull("", document, element);
            }
            if(ele instanceof JSONObject) {
                buildXMLForJSONObject((JSONObject)ele, "", document, element);
            }
            if(ele instanceof JSONArray) {
                buildXMLForJSONArray((JSONArray)ele, document, element);
            }
            if(ele instanceof String) {
                buildXMLForString((String)ele, "", document, element);
            }
            if(ele instanceof Number) {
                buildXMLForNumber((Number)ele, "", document, element);
            }
            if(ele instanceof Boolean) {
                buildXMLForBoolean((Boolean)ele, "", document, element);
            }
        });
    }

    private void buildXMLForJSONObject(JSONObject jsonObject, String key, Document document, Element rootElement) {
        Element element = createElementAndAttributes(document, key, "object");
        rootElement.appendChild(element);
        Iterator<String> keys = jsonObject.keySet().iterator();
        while(keys.hasNext()) {
            String key2 = keys.next();
            buildXMLFromJSON(jsonObject, key2, document, element);
        }
    }

    private Element createElementAndAttributes(Document document, String key, String elementType) {
        Element element = document.createElement(elementType);
        if(!key.isEmpty()) {
            Attr attr = document.createAttribute("name");
            attr.setValue(key);
            element.setAttributeNode(attr);
        }
        return element;
    }
}
