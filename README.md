# Json2XML

1. Run the mvn clean install command to download the dependencies.
2. Run mvn clean package to create a fat jar using the shade plugin.
3. This Jar can be found in the target folder.
4. Run the Jar with java -jar Json2XML-1.0-SNAPSHOT.jar command
5. Command Line will ask for the json file and xml file names. For the JSON file name enter example.json. This file is placed in the resource folder of the project and will be available in the classpath in the jar file. XML file name is optional. Currently the generated XML content will be printed in the console. If you need a generated XML uncomment line 52 and Comment line 53 in Json2XMLConverter.java. Provide an absolute path of the directory where the XML file needs to be generated along with the file name.
